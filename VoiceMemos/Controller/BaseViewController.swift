//
//  BaseViewController.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 4/25/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
	deinit {
		print("\(self) dealloc")
	}
	
	override func viewDidLoad() {
		NSNotificationCenter.defaultCenter().addObserver(self,
		                                                 selector: #selector(rotated(_:)),
		                                                 name: UIDeviceOrientationDidChangeNotification,
		                                                 object: nil)
	}
	
	func rotated (notification: NSNotification) {
		// NOTHING TO-DO
	}
	
	func applicationWillResignActive(application: UIApplication) {

	}
	
	func applicationDidEnterBackground(application: UIApplication) {

	}
	
	func applicationWillEnterForeground(application: UIApplication) {
		
	}
	
	func applicationDidBecomeActive(application: UIApplication) {

	}
	
	func applicationWillTerminate(application: UIApplication) {

	}
}
