//
//  RecordViewController.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 4/25/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation
import Darwin

enum PlayStatus: Int, CustomStringConvertible {
	case Play = 0,
	Pause

	var audioName: String {
		let audioNames = [
			"Audio:Play",
			"Audio:Pause"]
		return audioNames[rawValue]
	}

	var description: String {
		return audioName
	}
}

class RecordViewController: UIViewController {

	@IBOutlet weak var playButton: UIButton!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var replayButton: UIButton!
	@IBOutlet weak var newRecordButton: UIButton!
	@IBOutlet weak var saveButton: UIButton!
	@IBOutlet weak var listButton: UIButton!
	@IBOutlet weak var bottomBackgroundView: UIView!
	@IBOutlet weak var circleBackgroundView: UIView!

	// var
	var playStatus: PlayStatus!
	var replayStatus: PlayStatus!
	var isStopAnimation: Bool!

	override func viewDidLoad() {
		super.viewDidLoad()
		RecordHelper.sharedInstance.delegate = self
		RecordHelper.sharedInstance.configuration()
		RecordHelper.sharedInstance.setupRecorder()
		
		// set up layout
		saveButton.enabled = false
		replayButton.enabled = false
		newRecordButton.enabled = false
	}

	deinit {
		print("\(self) dealloc")
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		bottomBackgroundView.backgroundColor = UIColor(hex: 0x0000, alpha: 0.3)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	@IBAction func onPlayTouchUpInside(sender: UIButton) {
		RecordHelper.sharedInstance.pauseReply()	 // when user touch reply, stop now
		// hình trên button là play
		if playStatus == nil || playStatus == .Play {
			isStopAnimation = false
			waveAnimation()
			RecordHelper.sharedInstance.record()
			playStatus = .Pause
			playButton.setBackgroundImage(UIImage(named: "bigStop"), forState: UIControlState.Normal)
			updateUIPlaying()

		} else if playStatus == .Pause {
			// hình trên button là pause
			isStopAnimation = true
			RecordHelper.sharedInstance.pause()
			playStatus = .Play
			playButton.setBackgroundImage(UIImage(named: "bigPlay"), forState: UIControlState.Normal)
			updateUIPause()
		}
	}

	@IBAction func onReplayTouchUpInside(sender: UIButton) {
		if RecordHelper.sharedInstance.audioStatus == .Playing {
			RecordHelper.sharedInstance.pauseReply()
		} else {
			RecordHelper.sharedInstance.play()
		}
	}

	@IBAction func onNewRecordTouchUpInside(sender: UIButton) {
		if timeLabel.text != "00:00:00" {
			let alertController = UIAlertController(title: "Warning",
			                                        message: "Do you want to new recording",
			                                        preferredStyle: .Alert)

			// cancel button
			alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action: UIAlertAction) in }))

			// OK button
			alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
				RecordHelper.sharedInstance.pauseReply()				
				RecordHelper.sharedInstance.setupRecorder()
				self.timeLabel.text = "00:00:00"
				self.playButton.setBackgroundImage(UIImage(named: "bigPlay"), forState: UIControlState.Normal)
			}))
			self.presentViewController(alertController, animated: true, completion: nil)
		}
	}

	@IBAction func onSaveTouchUpInside(sender: UIButton) {
		showPopUpSave()
	}

	@IBAction func onListTouchUpInside(sender: AnyObject) {
		RecordHelper.sharedInstance.pauseReply()
		self.dismissViewControllerAnimated(true, completion: nil)
	}
}

// MARK: - Background mode
extension RecordViewController {
	func applicationDidEnterBackground(application: UIApplication) {
		isStopAnimation = true
	}
	
	func applicationDidBecomeActive(application: UIApplication) {
		if playStatus == .Pause {
			isStopAnimation = false
			waveAnimation()
		}
	}
}

extension RecordViewController: RecordDelegate {
	func record(record: RecordHelper, handleInterruption notification: NSNotification) {

	}
	func record(record: RecordHelper, handleRouteChange notification: NSNotification) {

	}

	@objc func record(record: RecordHelper,
	                     audioRecorderDidFinishRecording recorder: AVAudioRecorder,
	                                                     successfully flag: Bool) {

	}

	func record(record: RecordHelper,
	                     audioPlayerDidFinishPlaying player: AVAudioPlayer,
	                                                 successfully flag: Bool) {

	}

	func record(record: RecordHelper, updateTime time: String) {
		timeLabel.text = time
	}

	func record(record: RecordHelper, stopUpdateTime time: String) {
		timeLabel.text = time
	}

}

// MARK: - Private method
extension RecordViewController {
	private func cleanFileName(name: String) -> String {
		let newName = name.stringByReplacingOccurrencesOfString(" ", withString: "")
		return newName
	}

	private func clickSaveButtonWithName(name: String) -> Bool {
		let cleanName = self.cleanFileName(name)
		let tuple = RecordHelper.sharedInstance.saveVoiceWithName(cleanName)
		// check without error
		if tuple.error == nil {
			// save to Core Date
			CoreDataHelper.saveRecord(name,
			                          date: tuple.date,
			                          length: self.timeLabel.text!,
			                          fileName: cleanName)
			print("Sucessfull save file")
			return true
		} else {
			// error
			print("Error save file")
			return false
		}
	}

	private func clickDeleteButton(fileName: String) {
		// TO-DO
		UIAlertController.showAlertWithMessage(self,
		                                       title: "Delete Recording",
		                                       message: "Are you sure you want to delete '\(fileName)'",
		                                       firstButtonMessage: "Cancel",
		                                       secondButtonMessage: "Delete",
		                                       firstButtonType: UIAlertActionStyle.Cancel,
		                                       secondButtonType: UIAlertActionStyle.Destructive,
		                                       firstCallback: { (action: UIAlertAction) in
																						// Cancel
		}) { (action: UIAlertAction?) in
			// Delete
			print("Remove file tmp : \(FileManager.removeFileTmp())")
		}
	}


	private func showPopUpSave() {
		var inputTextField: UITextField?
		// create alertViewController
		let alertViewController = UIAlertController(title: "Save Voice Memo",
		                                            message: "",
		                                            preferredStyle: .Alert)
		alertViewController.addTextFieldWithConfigurationHandler { (textfield: UITextField) in
			// add file name
			textfield.text = "New Recording"
			textfield.placeholder = "New Recording"
			inputTextField = textfield
		}

		// delete button tap
		alertViewController.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction) in

			print("User click Delete button")
			self.clickDeleteButton(inputTextField!.text!)
		}))

		// save button tap
		alertViewController.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction) in
			print("User click Save button")
			let beSucessSaveFile = self.clickSaveButtonWithName((inputTextField?.text!)!)
			if beSucessSaveFile {
				self.dismissViewControllerAnimated(true, completion: nil)
			} else {
				// TO-DO
			}
		}))
		self.presentViewController(alertViewController, animated: true, completion: nil)
	}

	private func updateUIPlaying() {
		newRecordButton.enabled = false
		saveButton.enabled = false
		listButton.enabled = false
		replayButton.enabled = false
	}

	private func updateUIPause() {
		newRecordButton.enabled = true
		saveButton.enabled = true
		listButton.enabled = true
		replayButton.enabled = true
	}
}

// MARK: - Wave Animation
extension RecordViewController {
	private func waveAnimation() {
		let groupAnimation = groupAnimationWithValue("Group1", beginTime: CACurrentMediaTime())
		let templeView = viewAnimationWithTag(1024)
		templeView.layer.addAnimation(groupAnimation, forKey: nil)
		
		let groupAnimation2 = groupAnimationWithValue("Group2", beginTime: CACurrentMediaTime() + 0.25)
		let templeView2 = viewAnimationWithTag(1025)
		templeView2.layer.addAnimation(groupAnimation2, forKey: nil)
		
		let groupAnimation3 = groupAnimationWithValue("Group3", beginTime: CACurrentMediaTime() + 0.5)
		let templeView3 = viewAnimationWithTag(1026)
		templeView3.layer.addAnimation(groupAnimation3, forKey: nil)
	}
	
	private func viewAnimationWithTag(tag: Int) -> UIView {
		let templeView = UIView(frame: playButton.frame)
		view.insertSubview(templeView, belowSubview: playButton)
		templeView.backgroundColor = UIColor.lightGrayColor()
		templeView.tag = tag
		templeView.alpha = 0.5
		templeView.layer.cornerRadius = templeView.frame.width / 2
		return templeView
	}
	
	private func groupAnimationWithValue(name: String, beginTime: CFTimeInterval) -> CAAnimationGroup {
		let groupAnimation = CAAnimationGroup()
		groupAnimation.beginTime = beginTime
		groupAnimation.duration = 1
		groupAnimation.fillMode = kCAFillModeBackwards
		groupAnimation.animations = [scaleAnimation(), opacityAnimation()]
		groupAnimation.delegate = self
		groupAnimation.setValue(name, forKey: "name")
		return groupAnimation
	}
	
	private func scaleAnimation() -> CABasicAnimation {
		let animation = CABasicAnimation(keyPath: "transform.scale")
		animation.fromValue = 1.0
		animation.toValue = 2.5
		return animation
	}
	
	private func opacityAnimation() -> CABasicAnimation {
		let animation = CABasicAnimation(keyPath: "opacity")
		animation.fromValue = 0.5
		animation.toValue = 0.0
		return animation
	}
	
	override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
		print("animation did finish")
		if let name = anim.valueForKey("name") as? String {
			if name == "Group1" {
				// templeView
				for subView in self.view.subviews {
					if subView.tag == 1024 {
						subView.removeFromSuperview()
					}
				}
			}
			
			if name == "Group2" {
				// templeView
				for subView in self.view.subviews {
					if subView.tag == 1025 {
						subView.removeFromSuperview()
					}
				}
			}
			
			if name == "Group3" {
				// templeView
				for subView in self.view.subviews {
					if subView.tag == 1026 {
						subView.removeFromSuperview()
					}
				}
				// will continues animation
				if !isStopAnimation {
					waveAnimation()
				}
			}
		}
	}
}

