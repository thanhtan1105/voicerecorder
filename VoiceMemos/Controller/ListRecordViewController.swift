//
//  ListRecordViewController.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 4/25/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation

class ListRecordViewController: BaseViewController {

	@IBOutlet weak var searchView: UIView!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var bottomBackgroundView: UIView!
	@IBOutlet weak var circleBackgroundView: HalftCircleView!

	// var
	var searchController = UISearchController(searchResultsController: nil)
	var voiceList = [Voice]()
	var searchVoiceList = [Voice]()
	var currentExpend: NSIndexPath!

	// let
	let fetchQueue = dispatch_queue_create("com.fetchQueue", DISPATCH_QUEUE_CONCURRENT)

	override func viewDidLoad() {
		super.viewDidLoad()
		searchController = ({
			// Setup the Search Controller
			let controller = UISearchController(searchResultsController: nil)
			controller.searchResultsUpdater = self
			controller.searchBar.delegate = self
			definesPresentationContext = true
			controller.dimsBackgroundDuringPresentation = false
			controller.searchBar.tintColor = UIColor.whiteColor()
			let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSingleTap(_:)))
			tapGesture.cancelsTouchesInView = false
			self.view.addGestureRecognizer(tapGesture)
			searchView.addSubview(controller.searchBar)
			return controller
		})()
	}

	override func viewWillAppear(animated: Bool) {
		// update UI
		bottomBackgroundView.backgroundColor = UIColor(hex: 0x0000, alpha: 0.3)
		currentExpend = NSIndexPath()

		// fetch Data in background tread
		dispatch_async(fetchQueue) {
			let voiceArr = CoreDataHelper.fetchRecordWithName("")
			self.voiceList = voiceArr

			dispatch_async(dispatch_get_main_queue(), {
				self.tableView.reloadData()
			})
		}
	}
	
	override func rotated(notification: NSNotification) {
		
	}
	
	override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
		coordinator.animateAlongsideTransition({ (context: UIViewControllerTransitionCoordinatorContext) in
			self.searchController.searchBar.sizeToFit()
			}, completion: nil)
	}
}

// MARK: - Private Method
extension ListRecordViewController {
	@objc private func handleSingleTap(gesture: UITapGestureRecognizer) {
		searchController.searchBar.endEditing(true)
	}
}

// MARK: - TableView
extension ListRecordViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("recordCell") as! RecordTableViewCell

		// search mode
		if searchController.active && searchController.searchBar.text != "" {
			cell.voice = searchVoiceList[indexPath.row]
		} else {
			cell.voice = voiceList[indexPath.row]
		}
		cell.parentController = self
		cell.delegate = self
		return cell
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if searchController.active && searchController.searchBar.text != "" {
			return searchVoiceList.count
		}
		return voiceList.count
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let cell = tableView.cellForRowAtIndexPath(indexPath) as! RecordTableViewCell
		cell.indexPath = indexPath

		tableView.deselectRowAtIndexPath(indexPath, animated: true)
		tableView.beginUpdates()
		currentExpend = indexPath
		tableView.endUpdates()
	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		if (currentExpend != nil) && indexPath == currentExpend {
			return 170
		}
		return 70
	}
}

// MARK: - Delegate RecordTableViewCellDelegate
extension ListRecordViewController: RecordTableViewCellDelegate {
	func recordTableViewCell(recordTableViewCell: UITableViewCell, didDeleteVoiceAtIndexPath indexPath: NSIndexPath) {
		if currentExpend == indexPath {
			currentExpend = NSIndexPath()
		}

		voiceList.removeAtIndex(indexPath.row)
		tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
	}
}

// MARK: - Delegate UISearch
extension ListRecordViewController: UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {

	/**
	Called when the search bar becomes the first responder or when the user makes changes inside the search bar
	- parameter searchController: searchController
	*/
	func updateSearchResultsForSearchController(searchController: UISearchController) {
		// filter mode is on
		if searchController.searchBar.text?.characters.count == 0 {
			self.view.alpha = 0.7
		} else {
			self.view.alpha = 1.0
		}
		filterContentForSearchText(searchController.searchBar.text!)
	}

	/**
	Query data search using Core Data
	- parameter searchText: what is user input in search bar
	*/
	func filterContentForSearchText(searchText: String) {
		searchVoiceList.removeAll()		// clear search data
		dispatch_async(fetchQueue) {
			let voiceArr = CoreDataHelper.fetchRecordWithName(searchText)
			self.searchVoiceList = voiceArr
			dispatch_async(dispatch_get_main_queue(), {
				self.tableView.reloadData()
			})
		}
	}

	/**
	Tells the delegate when the user begins editing the search text.
	- parameter searchBar:
	*/
	func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
		print("searchBarTextDidBeginEditing")
		self.view.alpha = 1.0
	}

	/**
	Tells the delegate that the cancel button was tapped
	- parameter searchBar:
	*/
	func searchBarCancelButtonClicked(searchBar: UISearchBar) {
		self.searchController.active = false
		self.view.alpha = 1.0
		currentExpend = NSIndexPath()
	}
}
