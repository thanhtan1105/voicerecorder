//
//  HalftCircleView.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 5/9/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class HalftCircleView: UIView {

    override func drawRect(rect: CGRect) {
			PaintcodeView.drawHalfCircle(frame: self.bounds)
			self.alpha = 0.3
    }
}
