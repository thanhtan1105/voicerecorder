//
//  RecordTableViewCell.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 5/4/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation

protocol RecordTableViewCellDelegate: class {
	func recordTableViewCell(recordTableViewCell: UITableViewCell,
	                         didDeleteVoiceAtIndexPath indexPath: NSIndexPath)
}

class RecordTableViewCell: UITableViewCell {

	@IBOutlet weak var recordBackgroundView: UIView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var lengthLabel: UILabel!
	@IBOutlet weak var playButton: UIButton!
	@IBOutlet weak var silder: UISlider!
	@IBOutlet weak var shareButton: UIButton!
	@IBOutlet weak var deleteButton: UIButton!
	@IBOutlet weak var currentTimeLabel: UILabel!
	
	weak var parentController = UIViewController()
	weak var delegate: RecordTableViewCellDelegate!
	var indexPath = NSIndexPath()
	var audioPlayer = AVAudioPlayer()
	var playStatus: PlayStatus!
	var audioConverter = TPAACAudioConverter()	// convert file
	var voice: Voice! {
		didSet {
			nameLabel.text = voice.name // name
			let formatter = NSDateFormatter()
			formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
			let timeString = formatter.stringFromDate(voice.date!)
			dateLabel.text = timeString // date
			lengthLabel.text = voice.length // length
			configureAudio() // configure
		}
	}

	override func awakeFromNib() {
		self.backgroundColor = UIColor.clearColor()
	}

	// MARK: - IBAction
	@IBAction func onPlayTapped(sender: UIButton) {
		if playStatus == nil || playStatus == .Pause {

			audioPlayer.play()
			playStatus = .Play
			playButton.setImage(UIImage(named: "stop"), forState: UIControlState.Normal)
		} else {

			audioPlayer.pause()
			playStatus = .Pause
			playButton.setImage(UIImage(named: "play"), forState: UIControlState.Normal)
		}
	}

	@IBAction func onShareTapped(sender: UIButton) {
		let urlPath = getURLforMemoReturnString()
		let templeDictory = NSTemporaryDirectory()
		let destinationFile = templeDictory + "\(voice.fileName!).m4a"
		audioConverter = TPAACAudioConverter(delegate: self, source: urlPath, destination: destinationFile)
		audioConverter.start()
	}

	@IBAction func onDeleteTapped(sender: UIButton) {
		// require
		let sheetVC = UIAlertController(title: "",
		                                message: "Do you want to delete",
		                                preferredStyle: .ActionSheet)
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action: UIAlertAction) in
			// nothing
		}
		
		let deleteAction = UIAlertAction(title: "Delete", style: .Destructive) { (action: UIAlertAction) in
			CoreDataHelper.deleteRecord(self.voice)
			// pause when playing
			self.audioPlayer.pause()
			self.playStatus = .Pause
			// send delegate to viewcontroller
			self.delegate.recordTableViewCell(self, didDeleteVoiceAtIndexPath: self.indexPath)
		}
		sheetVC.addAction(cancelAction)
		sheetVC.addAction(deleteAction)
		if let presenter = sheetVC.popoverPresentationController {
			presenter.sourceView = deleteButton
			presenter.sourceRect = deleteButton.bounds
		}
		parentController!.presentViewController(sheetVC, animated: true, completion: nil)
	}

	@IBAction func onSilderTapped(sender: UISlider) {
		audioPlayer.currentTime = Double(sender.value)
	}
}

// MARK: - Private Method
extension RecordTableViewCell {
	private func getURLforMemo() -> NSURL {
		let pathArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
		                                                    .UserDomainMask,
		                                                    true)
		let documentDirectory = pathArray[0]
		let dataPath = documentDirectory.stringByAppendingString("/Audio")

		// get date folder
		let formatter = NSDateFormatter()
		formatter.dateFormat = "yyyyMMddHHmmss"
		let dateString = formatter.stringFromDate(voice.date!)
		let dateFolder = dataPath.stringByAppendingString("/\(dateString)")

		// get file String
		let fileString = dateFolder.stringByAppendingString("/\(voice.fileName!).caf")
		return NSURL(string: fileString)!
	}
	
	private func getURLforMemoReturnString() -> String {
		let pathArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
		                                                    .UserDomainMask,
		                                                    true)
		let documentDirectory = pathArray[0]
		let dataPath = documentDirectory.stringByAppendingString("/Audio")
		
		// get date folder
		let formatter = NSDateFormatter()
		formatter.dateFormat = "yyyyMMddHHmmss"
		let dateString = formatter.stringFromDate(voice.date!)
		let dateFolder = dataPath.stringByAppendingString("/\(dateString)")
		
		// get file String
		let fileString = dateFolder.stringByAppendingString("/\(voice.fileName!).caf")
		return fileString
	}


	@objc private func updateTime() {
		let currentTime = Int(audioPlayer.currentTime)

		let hour = currentTime / 3600
		let minutes = (currentTime / 60) % 60
		let seconds = currentTime % 60

		currentTimeLabel.text = String(format: "%02i:%02i:%02i",
		                               arguments: [hour, minutes, seconds])
	}

	@objc private func updateSlider() {
		silder.value = Float(audioPlayer.currentTime)
	}

	/**
	Description: Configure audio
	*/
	private func configureAudio() {
		let pathURL = getURLforMemo()
		print("PathURL: \(pathURL)")
		do {
			try audioPlayer = AVAudioPlayer(contentsOfURL: pathURL)
			audioPlayer.delegate = self
			silder.maximumValue = Float(audioPlayer.duration)
			silder.value = Float(audioPlayer.currentTime)
		} catch let error as NSError {
			print("Error: \(error)")
		}

		NSTimer.scheduledTimerWithTimeInterval(0.01,
		                                       target: self,
		                                       selector: #selector(updateTime),
		                                       userInfo: nil,
		                                       repeats: true)
		
		NSTimer.scheduledTimerWithTimeInterval(0.01,
		                                       target: self,
		                                       selector: #selector(updateSlider),
		                                       userInfo: nil,
		                                       repeats: true)
	}
}

extension RecordTableViewCell: AVAudioPlayerDelegate {
	func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
		playButton.setImage(UIImage(named: "play"), forState: UIControlState.Normal)
		playStatus = .Pause
	}
}

extension RecordTableViewCell: TPAACAudioConverterDelegate {
	func AACAudioConverterDidFinishConversion(converter: TPAACAudioConverter!) {
		NSNotificationCenter.defaultCenter().removeObserver(self)
		let templeDic = NSTemporaryDirectory()
		let destinationFile = templeDic + "\(voice.fileName!).m4a"
		let file = NSURL.fileURLWithPath(destinationFile)
		let activityViewController = UIActivityViewController(activityItems: [file], applicationActivities: nil)
		if let presenter = activityViewController.popoverPresentationController {
			presenter.sourceView = shareButton
			presenter.sourceRect = shareButton.bounds
		}
		parentController!.presentViewController(activityViewController, animated: true, completion: nil)

	}
	
	func AACAudioConverter(converter: TPAACAudioConverter!, didMakeProgress progress: Float) {
		print("Make Process")
	}
	
	func AACAudioConverter(converter: TPAACAudioConverter!, didFailWithError error: NSError!) {
		print("Fail with error: \(error.userInfo)")
	}
}
