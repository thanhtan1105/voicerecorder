//
//  CoreDataHelper.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 5/4/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHelper {
	private static var managedContext: NSManagedObjectContext = CoreDataStack().context
	
	// Insert code here to add functionality to your managed object subclass
	class func saveRecord(name: String, date: NSDate, length: String, fileName: String) {
		let voiceEntity = NSEntityDescription.entityForName("Voice", inManagedObjectContext: managedContext)
		let voice = Voice(entity: voiceEntity!, insertIntoManagedObjectContext: managedContext)
		voice.date = date
		voice.fileName = fileName
		voice.length = length
		voice.name = name
		
		// Save the managed object context
		do {
			try managedContext.save()
		} catch let error as NSError {
			print("Could not save:\(error)")
		}
	}
	
	class func deleteRecord(voice: Voice) {
		managedContext.deleteObject(voice)
		
		do {
			try managedContext.save()
		} catch let error as NSError {
			print("Could not delete:\(error) ")
		}
	}
	
	class func fetchRecordWithName(name: String) -> [Voice] {
		let voiceFetch = NSFetchRequest(entityName: "Voice")
		if name.characters.count > 0 {
			voiceFetch.predicate = NSPredicate(format: "name CONTAINS[cd] %@", name)
		}
		
		do {
			let result = try managedContext.executeFetchRequest(voiceFetch) as! [Voice]
			return result
		} catch let error as NSError {
			print("Error: \(error) " +
				"description \(error.localizedDescription)")
		}
		
		return []
	}
}
