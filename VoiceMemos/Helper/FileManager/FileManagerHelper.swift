//
//  FileManagerHelper.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 5/5/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class FileManager {
	/**
	Function: support to get document directory
	
	- returns: (String) link document directory
	*/
	class func getDocumentDirectory() -> String {
		let pathArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
		let documentDirectory = pathArray[0]
		return documentDirectory
	}
	
	/**
	Function: support to get audio directory
	
	- returns: (String) audio document directory
	*/
	class func getAudioDirectory() -> String {
		let documentDirectory = getDocumentDirectory()
		let dataPath = documentDirectory.stringByAppendingString("/Audio")
		if !NSFileManager.defaultManager().fileExistsAtPath(dataPath) {
			do {
				try NSFileManager.defaultManager().createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
			} catch let error as NSError {
				print(error)
			}
		}
		return dataPath
	}
	
	/**
	Function: create folder with name equal date on Audio directory. Format date is 'yyyyMMddHHmmss'
	
	- parameter date: NSDate
	
	- returns: Tuple with 2 param (error: NSError?, linkString: String). It have 2 status:
		1. Sucess: return (nil, dateDirectory)
		2. Fail: return (error, "")
	*/
	class func createNewFolderWithDate(date: NSDate) -> (error: NSError?, linkString: String) {
		// parse date to yyymmddhhMMss
		let formatter = NSDateFormatter()
		formatter.dateFormat = "yyyyMMddHHmmss"
		let dateString = formatter.stringFromDate(date)
		let audioDirectory = getAudioDirectory()
		let dateDirectory = audioDirectory.stringByAppendingString("/\(dateString)")
		
		if !NSFileManager.defaultManager().fileExistsAtPath(dateDirectory) {
			do {
				try NSFileManager.defaultManager().createDirectoryAtPath(dateDirectory,
				                                                         withIntermediateDirectories: false,
				                                                         attributes: nil)
				return (nil, dateDirectory)
			} catch let error as NSError {
				print("Error loading create folder")
				return (error, "")
			}
		}
		print("Folder exist")
		return (nil, dateDirectory)
	}
	
	/**
	Function: get file record on date directory. Combine date(format 'yyyyMMddHHmmss') and fileName to get file on directory. Example: Document/Audio/yyyyMMddHHmmss/fileName.caf
	
	- parameter date:     String(fromat 'yyyyMMddHHmmss')
	- parameter fileName: FileName in coreData
	
	- returns: NSURL: that is link
	*/
	class func getURLRecordFromDateDirectory(date: String, fileName: String) -> NSURL {
		// format date yyyyMMddHHmmss
		let audioDirectory = getAudioDirectory()
		let dateDirectory = audioDirectory.stringByAppendingString("/\(date)")
		let filePath = dateDirectory.stringByAppendingString("/\(fileName)")
		return NSURL.fileURLWithPath(filePath)
	}
	
	class func removeFileTmp() -> Bool {
		let tempDir = NSTemporaryDirectory()
		let filePath = tempDir + "/TempMemo.caf"
		do {
			try NSFileManager.defaultManager().removeItemAtURL(NSURL.fileURLWithPath(filePath))
		} catch let error as NSError {
			print("Error remove file tmp: \(error)")
			return false
		}
		return true
	}
	
	class func getURLforMemoTemporary() -> NSURL {
		let tempDir = NSTemporaryDirectory()
		let filePath = tempDir + "/TempMemo.caf"
		return NSURL.fileURLWithPath(filePath)
	}
}
