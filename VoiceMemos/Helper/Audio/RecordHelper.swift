//
//  AudioHelper.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 4/26/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation


enum AudioStatus: Int, CustomStringConvertible {
	case Stopped = 0,
	Playing,
	Recording,
	Pause

	var audioName: String {
		let audioNames = [
			"Audio:Stopped",
			"Audio:Playing",
			"Audio:Recording",
			"Audio:Pause"]
		return audioNames[rawValue]
	}

	var description: String {
		return audioName
	}
}

@objc
protocol RecordDelegate: class {
	func record(record: RecordHelper, handleInterruption notification: NSNotification)
	func record(record: RecordHelper, handleRouteChange notification: NSNotification)
	optional func record(record: RecordHelper,
	            audioRecorderDidFinishRecording recorder: AVAudioRecorder,
	                                            successfully flag: Bool)
	optional func record(record: RecordHelper,
	            audioPlayerDidFinishPlaying player: AVAudioPlayer,
	                                            successfully flag: Bool)

	optional func record(record: RecordHelper, updateTime time: String)
	optional func record(record: RecordHelper, stopUpdateTime time: String)
}

class RecordHelper: NSObject, AVAudioPlayerDelegate, AVAudioRecorderDelegate {

	// MARK: - Public variable
	let session = AVAudioSession.sharedInstance()
	var appHasMicAccess = true	// check app has mic
	weak var delegate: RecordDelegate! // delegate
	var volume: Float = 1 {
		didSet {
			audioPlayer.volume = volume
		}
	} // volume

	var pan: Float = 0 {
		didSet {
			audioPlayer.pan = pan
		}
	} // pan

	static var sharedInstance: RecordHelper = {
		return RecordHelper()
	}()

	// MARK: - Private variable
	var audioStatus: AudioStatus = AudioStatus.Stopped
	private var audioRecorder: AVAudioRecorder!
	private var audioPlayer: AVAudioPlayer!
	private let notification = NSNotificationCenter.defaultCenter()
	private var soundTimer: CFTimeInterval = 0.0
	private var updateTimer: CADisplayLink!

	// MARK: - Delegate
	func audioRecorderDidFinishRecording(recorder: AVAudioRecorder, successfully flag: Bool) {
		delegate.record!(self, audioRecorderDidFinishRecording: recorder, successfully: flag)
		audioStatus = .Stopped

	}

	func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
		delegate.record!(self, audioPlayerDidFinishPlaying: player, successfully: flag)
		audioStatus = .Stopped
	}

	@objc private func handleInterruption(notification: NSNotification) {
		if let delegate = delegate {
			delegate.record(self, handleInterruption: notification)
		}
	}

	@objc private func handleRouteChange(notification: NSNotification) {
		if let delegate = delegate {
			delegate.record(self, handleRouteChange: notification)
		}
	}
}

// MARK: - Public method
extension RecordHelper {
	func configuration() {
		do {
			try session.setCategory(AVAudioSessionCategoryPlayAndRecord, withOptions: AVAudioSessionCategoryOptions.DefaultToSpeaker)
			try session.setActive(true)

			// Check for microphone permission..
			session.requestRecordPermission({ (granted: Bool) in
				self.appHasMicAccess = granted
				self.notification.addObserver(self, selector: #selector(self.handleInterruption(_:)), name: AVAudioSessionInterruptionNotification, object: self.session)
				self.notification.addObserver(self, selector: #selector(self.handleRouteChange(_:)), name: AVAudioSessionRouteChangeNotification, object: self.session)
			})
		} catch let error as NSError {
			print("AVAudioSession configuration error : \(error.localizedDescription)")
		}
	}

	func setupRecorder() {
		_ = removeFileTmp()
		let url = getURLforMemoTemporary()
		let recordSetting = [
			AVFormatIDKey: Int(kAudioFormatLinearPCM),
			AVSampleRateKey: 44100.0,
			AVNumberOfChannelsKey: 1,
			AVEncoderAudioQualityKey: AVAudioQuality.High.rawValue
		]

		do {
			audioRecorder = try AVAudioRecorder(URL: url, settings: recordSetting as! [String: AnyObject])
			audioRecorder.delegate = self
			audioRecorder.prepareToRecord()
		} catch {
			print("Error loading audioPlayer.")
		}
	}

	func record() {
		audioRecorder.record()
		audioStatus = .Recording
		startUpdateLoop()
	}

	func pause() {
		audioRecorder.pause()
		audioStatus = .Pause
	}

	func resume() {
		// resume meaning is record
		record()
	}

	// function stop recording
	func stopRecording() {
		audioRecorder.stop()
		audioStatus = .Stopped
		stopUpdateLoop()
	}

	func pauseReply() {
		if let audioPlayer = audioPlayer {
			audioPlayer.pause()
			audioStatus = .Pause
		}
	}
	
	// function play audio
	func play() {
		let fileURL = getURLforMemoTemporary()
		do {
			audioPlayer = try AVAudioPlayer(contentsOfURL: fileURL)
			audioPlayer.delegate = self
			if audioPlayer.duration > 0.0 {
				audioPlayer.play()
				audioStatus = .Playing
			}
		} catch {
			print("Error loading audioPlayer.")
		}
	}

	func saveVoiceWithName(fileName: String) -> (date: NSDate, error: NSError?) {
		// copy file in temporary to document
		let dirPaths = getURLforMemoTemporary()
		let date = NSDate()
		let tuple = FileManager.createNewFolderWithDate(date)

		// check without error
		if tuple.error == nil {
			let directory = tuple.linkString
			let newFile = directory.stringByAppendingString("/\(fileName).caf")
			do {
				// change file name and format
				try NSFileManager.defaultManager().copyItemAtURL(dirPaths, toURL: NSURL(fileURLWithPath: newFile))

				removeFileTmp()
				return (date: date, error: nil)
			} catch let error as NSError {
				print("Error move file: \(error)")
				return (date: date, error: error)
			}



		} else {
			// have error
			return (date: date, error: NSError(domain: "com.voice", code: 10, userInfo: ["error" : tuple.error!]))
		}
	}
}

// MARK: - Private method
extension RecordHelper {

	private func startUpdateLoop() {
		if updateTimer != nil {
			updateTimer.invalidate()
		}
		updateTimer = CADisplayLink(target: self, selector: #selector(updateLoop))
		updateTimer.frameInterval = 1
		updateTimer.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
	}

	@objc private func updateLoop() {
		if audioStatus == .Recording {
			if CFAbsoluteTimeGetCurrent() - soundTimer > 0.5 {
				let time = formattedCurrentTime(UInt(audioRecorder.currentTime))
				if let delegate = delegate {
					delegate.record!(self, updateTime: time)
					soundTimer = CFAbsoluteTimeGetCurrent()
				}
			}
		}
	}

	private func stopUpdateLoop() {
		updateTimer.invalidate()
		updateTimer = nil
		// Update UI
		let time = formattedCurrentTime(UInt(0))
		delegate.record!(self, stopUpdateTime: time)
	}

	private func formattedCurrentTime(time: UInt) -> String {
		let hours = time / 3600
		let minutes = (time / 60) % 60
		let seconds = time % 60
		return String(format: "%02i:%02i:%02i", arguments: [hours, minutes, seconds])
	}


	private func removeFileTmp() -> Bool {
		let tempDir = NSTemporaryDirectory()
		let filePath = tempDir + "/TempMemo.caf"
		do {
			try NSFileManager.defaultManager().removeItemAtURL(NSURL.fileURLWithPath(filePath))
		} catch let error as NSError {
			print("Error remove file tmp: \(error)")
			return false
		}
		return true
	}

	private func getURLforMemoTemporary() -> NSURL {
		let tempDir = NSTemporaryDirectory()
		let filePath = tempDir + "/TempMemo.caf"
		return NSURL.fileURLWithPath(filePath)
	}
}
