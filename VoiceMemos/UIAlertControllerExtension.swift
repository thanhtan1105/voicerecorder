//
//  AlertControllerExtension.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 5/21/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

extension UIAlertController {
	class func showAlertWithMessage(viewController: UIViewController,
																	title: String,
	                                message: String,
	                                firstButtonMessage: String,
	                                secondButtonMessage: String?,
	                                firstButtonType: UIAlertActionStyle,
	                                secondButtonType: UIAlertActionStyle,
	                                firstCallback: (UIAlertAction) -> (),
	                                secondCallback: (UIAlertAction?) -> ()) {
		// require
		let alertVC = UIAlertController(title: title,
		                                message: message,
		                                preferredStyle: .Alert)

		let firstAction = UIAlertAction(title: firstButtonMessage,
		                                style: firstButtonType,
		                                handler: firstCallback)
		alertVC.addAction(firstAction)

		// optional
		if let secondMessage = secondButtonMessage {
			let secondAction = UIAlertAction(title: secondMessage,
			                                 style: secondButtonType,
			                                 handler: secondCallback)
			alertVC.addAction(secondAction)
		}

		// present
		viewController.presentViewController(alertVC, animated: true, completion: nil)
	}

	class func showSheetWithTwoAction(viewController: UIViewController,
																		title: String,
	                                  message: String,
	                                  cancelMessage: String,
	                                  firstMessage: String,
	                                  secondMessage: String?,
	                                  cancelCallback: (UIAlertAction) -> (),
	                                  firstCallback: (UIAlertAction) -> (),
	                                  secondCallback: (UIAlertAction) -> ()) {
		// require
		let sheetVC = UIAlertController(title: title,
		                                message: message,
		                                preferredStyle: .ActionSheet)

		let cancelAction = UIAlertAction(title: cancelMessage, style: .Cancel, handler: cancelCallback)
		let firstAction = UIAlertAction(title: firstMessage, style: .Destructive, handler: firstCallback)
		sheetVC.addAction(cancelAction)
		sheetVC.addAction(firstAction)

		// optional
		if let secondMessage = secondMessage {
			let secondAction = UIAlertAction(title: secondMessage, style: .Default, handler: secondCallback)
			sheetVC.addAction(secondAction)
		}

		// present
		viewController.presentViewController(sheetVC, animated: true, completion: nil)
	}
}
