//
//  Voice+CoreDataProperties.swift
//  VoiceMemos
//
//  Created by Le Thanh Tan on 4/25/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Voice {
    @NSManaged var name: String?
    @NSManaged var date: NSDate?
    @NSManaged var length: String?
    @NSManaged var fileName: String?
}
